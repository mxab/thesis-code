#!/opt/python-3.6.5/bin/python3.6

"""
This starts the training operation.

Requires a model_dir containing a params.json. Auto-allocates available GPUs.
"""

import argparse
import logging
import os
from models.utils import Params, set_logger
from models.training import train_and_evaluate
import GPUtil

parser = argparse.ArgumentParser()
parser.add_argument('--model_dir', default='experiments/test',
                    help="Experiment directory containing params.json")

parser.add_argument('--restore_from', default=None,
                    help="Optional, directory or file containing weights to reload before training")

parser.add_argument('--force_gpu', help="Optional, force the usage of specific GPU(s)", default=None)

if __name__ == '__main__':

    args = parser.parse_args()
    json_path = os.path.join(args.model_dir, 'params.json')
    assert os.path.isfile(json_path), "No json configuration file found at {}".format(json_path)
    params = Params(json_path)

    #  Determine which GPU is available
    availableGPUs = GPUtil.getAvailable(order='memory', limit=2, maxLoad=0.1, maxMemory=0.05)

    if args.force_gpu is not None:
        availableGPUs = args.force_gpu

    assert len(availableGPUs) >= params.req_num_gpus, "Not enough GPUs available, aborting"

    os.environ["CUDA_VISIBLE_DEVICES"] = str(availableGPUs[:params.req_num_gpus]).strip('[]')

    model_dir_has_best_weights = os.path.isdir(os.path.join(args.model_dir, "best_weights"))
    overwriting = model_dir_has_best_weights and args.restore_from is None
    assert not overwriting, "Weights found in model_dir, aborting to avoid overwrite"

    set_logger(os.path.join(args.model_dir, 'train.log'))

    logging.info("Starting training for {} epoch(s)".format(params.num_epochs))

    train_and_evaluate(params, args.model_dir, args.restore_from)
