import tensorflow as tf
import argparse
import logging
import os
from models.utils import Params, set_logger
from models.training import build_model, train_and_evaluate

params = Params('params.json')
os.environ["CUDA_VISIBLE_DEVICES"] = "1"
set_logger('train.log')
inpt = dict()
inpt["images"] = tf.zeros(shape=[1, 80, 144, 144, 2])
inpt["labels"] = tf.zeros(shape=[1, 80, 144, 144, 3])
tf.logging.set_verbosity(tf.logging.DEBUG)
logging.getLogger("tensorflow").setLevel(logging.DEBUG)
train_and_evaluate(params, '/home/d1266/no_backup/d1266/debug3/', None)
