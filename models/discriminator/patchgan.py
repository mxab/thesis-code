"""
All discriminator models are defined here.
"""

import tensorflow as tf

def spectral_norm(w, iteration=1):
    """
    Spectral Norm implementation taken from https://github.com/taki0112/Spectral_Normalization-Tensorflow
    """
    w_shape = w.shape.as_list()
    w = tf.reshape(w, [-1, w_shape[-1]])

    u = tf.get_variable("u", [1, w_shape[-1]], initializer=tf.random_normal_initializer(), trainable=False)

    u_hat = u
    v_hat = None

    for i in range(iteration):
       """
       power iteration
       Usually iteration = 1 will be enough
       """
       v_ = tf.matmul(u_hat, tf.transpose(w))
       v_hat = tf.nn.l2_normalize(v_)

       u_ = tf.matmul(v_hat, w)
       u_hat = tf.nn.l2_normalize(u_)

    u_hat = tf.stop_gradient(u_hat)
    v_hat = tf.stop_gradient(v_hat)

    sigma = tf.matmul(tf.matmul(v_hat, w), tf.transpose(u_hat))

    with tf.control_dependencies([u.assign(u_hat)]):
       w_norm = w / sigma
       w_norm = tf.reshape(w_norm, w_shape)

    return w_norm


def conv_block(x, filters, fsize=4, batchnorm=True, dilation=None, is_training=True, stride=1, name=None, relu=True, spectral=True):

    if dilation is None:
        dilation = 1

    with tf.name_scope(name):

        with tf.variable_scope(name):

            kernel = tf.get_variable(name="kernel", shape=[fsize, fsize, fsize, x.get_shape()[-1], filters], trainable=True)
            bias = tf.get_variable(name="bias", shape=[filters], initializer=tf.zeros_initializer(), trainable=True)

            if spectral:
                kernel = spectral_norm(kernel)

            if dilation is None or dilation == 1:
                net = tf.nn.conv3d(input=x, filter=kernel, strides=[1, stride, stride, stride, 1],
                                   padding='VALID', name='conv')
            else:
                net = tf.nn.conv3d(input=x, filter=tf.ones(shape=[1, 1, 1, x.get_shape()[-1], x.get_shape()[-1]]),
                                   strides = [1, 2, 2, 2, 1], padding='SAME', name='conv_stride')
                net = tf.nn.conv3d(input=net, filter=kernel, dilations=[1, dilation, dilation, dilation, 1],
                                   padding='VALID', name='conv')
            net = net + bias

            if relu:
                net = tf.nn.leaky_relu(net)

            if batchnorm:
                net = tf.layers.batch_normalization(net, training=is_training)

    return net


def dilated_block(x, filters, batchnorm=True, is_training=True, stride=1, dilations=1, activation=True, dropout=None, name=None, spectral=True):
    """ 3D dilated convolutions are weird in TF (1.10). See for more information: https://github.com/tensorflow/tensorflow/issues/8431"""

    with tf.variable_scope(name):

        kernel = tf.get_variable(name="kernel", shape=[4, 4, 4, x.get_shape()[-1], filters], trainable=True)
        bias = tf.get_variable(name="bias", shape=[filters], initializer=tf.zeros_initializer(), trainable=True)

        if spectral:
            kernel = spectral_norm(kernel)

        if dilations > 1:

            if stride > 1:
                k = dilations/stride
                s = 1
                if k < 0:
                    raise Exception
                x = x[:, ::stride, ::stride, ::stride, :]  # Manual subsampling since TF does not support dilation+stride
            else:
                s = 1
                k = dilations
        else:  # dil = 1
            k = 1
            s = stride

        net = tf.nn.convolution(input=x, filter=kernel, padding="SAME", dilation_rate=[k, k, k], strides=[s, s, s])
        net = net + bias

        if activation:
            net = tf.nn.leaky_relu(net)

        if batchnorm:
            net = tf.layers.batch_normalization(net, training=is_training)

        if dropout is not None:
            net = tf.layers.dropout(net, dropout, training=is_training)

    return net


def patchgan(net, is_training=True, filters=None, strides=None, fsize=1, batchnorm=None, spectral=True):

    if filters is None:
        filters = [64, 128]

    if strides is None:
        strides = [2, 1]

    if batchnorm is None:
        batchnorm = [False, True]

    for i in range(len(filters)):
        net = conv_block(net, filters[i], fsize=fsize, batchnorm=batchnorm[i], is_training=is_training, stride=strides[i],
                         name='layer{}'.format(i+1))

    logits = conv_block(net, 1, batchnorm=False, fsize=fsize, is_training=is_training, name='layer5', relu=False, spectral=True)
    outputs = tf.nn.sigmoid(net, name='output')

    return outputs, logits


def patchgan_multiscale(x, is_training=True, filters=None, strides=None, batchnorm=None):

    """ This returns 3 Discriminators. Figure out how to use them in the code later"""
    outputs = []
    logits = []
# Original Resolution
    with tf.variable_scope('original'):
        net1outputs, net1logits = patchgan(x, is_training=is_training, filters=filters, strides=strides,
                                           batchnorm=batchnorm)
    outputs.append(net1outputs)
    logits.append(net1logits)
# Downsample 2x with avgpool
    with tf.variable_scope('halfres'):
        x1 = tf.layers.average_pooling3d(x, pool_size=2, strides=2, padding='same')
        net2outputs, net2logits = patchgan(x1, is_training=is_training, filters=filters, strides=strides,
                                           batchnorm=batchnorm)
    outputs.append(net2outputs)
    logits.append(net2logits)
# Downsample by again 2x
    with tf.variable_scope('quarterres'):
        x2 = tf.layers.average_pooling3d(x1, pool_size=2, strides=2, padding='same')
        net3outputs, net3logits = patchgan(x2, is_training=is_training, filters=filters, strides=strides,
                                           batchnorm=batchnorm)
    outputs.append(net3outputs)
    logits.append(net3logits)

    return outputs, logits


def dilated_fcn(x, is_training=True):

    net = dilated_block(x, filters=16, batchnorm=False, stride=2, is_training=is_training, name='layer1')
    net = dilated_block(net, filters=16, is_training=is_training, dilations=2, stride=2, name='layer2')
    net = dilated_block(net, filters=16, is_training=is_training, dilations=4, stride=2, name='layer3')
    net = dilated_block(net, filters=16, is_training=is_training, stride=2, name='layer4')
    logits = dilated_block(net, filters=1, is_training=is_training, batchnorm=False, activation=False, name='output')
    outputs = tf.nn.sigmoid(net)

    return outputs, logits
