"""
Wrapper. Returns the requested discriminator function.
"""
def discriminator_fn(x, is_training, params):

    if params.discriminator_model == "patchgan":

        from .patchgan import patchgan

        return patchgan(x, is_training=is_training, filters=params.disc_filters, fsize=params.disc_fsize, strides=params.disc_strides,
                        batchnorm=params.disc_batchnorm, spectral=True)

    elif params.discriminator_model == "patchgan_dilated":

        from .patchgan import patchgan_dilated

        return patchgan_dilated(x, is_training=is_training, filters=params.disc_filters, strides=params.disc_strides,
                                batchnorm=params.disc_batchnorm, dilations=params.disc_dilations)

    elif params.discriminator_model == "patchgan_dilated_multiscale":

        from .patchgan import patchgan_dilated_multiscale

        return patchgan_dilated_multiscale(x, is_training=is_training, filters=params.disc_filters,
                                           strides=params.disc_strides, batchnorm=params.disc_batchnorm,
                                           dilations=params.disc_dilations)

    elif params.discriminator_model == "patchgan_multiscale":

        from .patchgan import patchgan_multiscale

        return patchgan_multiscale(x, is_training=is_training, filters=params.disc_filters, strides=params.disc_strides,
                                   batchnorm=params.disc_batchnorm)

    elif params.discriminator_model == "dilated_fcn":

        from .patchgan import dilated_fcn

        return dilated_fcn(x, is_training=is_training)

    else:
        raise Exception
