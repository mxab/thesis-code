""" Defines the training and export operations """

import tensorflow as tf
import os
from collections import namedtuple
from models.import_data import input_fn
from . import utils
from tqdm import tqdm
import logging as log
import numpy as np


def build_model(params, train_input_iterator, eval_input_iterator):
    """
    Builds the model and returns a namedtuple containing the model's tensors and operations.

    This requires the params, and iterators for both the training and the evaluation pipeline.

    is_training defines the current state of the model and whether it's trained or evaluated.
    The corresponding input iterator is automatically chosen.
    """

    model = namedtuple('Model',
                       'train_op, summary_op, update_metrics_op, metrics_values, metrics_init_op, global_step, '
                       'gen_loss, is_training, is_exporting, input_images, input_labels, generated_labels,'
                       'step_summary_op')

    with tf.name_scope("input_pipelines/"):
        is_training = tf.placeholder(tf.bool, name='is_training')
        model.is_training = is_training

        input_images, input_labels = tf.cond(is_training,
                                             lambda: train_input_iterator.get_next(),
                                             lambda: eval_input_iterator.get_next())

        model.input_images = input_images
        labels = tf.cast(tf.argmax(input_labels, axis=-1, name="labels"), tf.int32)
        model.input_labels = labels

        global_step = tf.train.get_or_create_global_step()
        model.global_step = global_step

    from models.generator import generator_fn
    from models.discriminator import discriminator_fn

    # Forward Propagation
    with tf.name_scope("generator"):
        with tf.device(params.gpu_gen):
            with tf.variable_scope("generator"):
                reg = tf.contrib.layers.l2_regularizer(scale=params.l2_reg)
                generator_outputs, generator_outputs_logits = generator_fn(input_images, is_training, params, reg=reg)
                predictions = tf.argmax(generator_outputs, axis=-1, name="predictions")
                model.generated_labels = predictions

    with tf.name_scope("discriminator"):
        with tf.device(params.gpu_disc):

            epoch_approx = tf.cast(global_step / params.train_data_size, tf.float32)

            # The following sections do all the noisy label operations.
            if params.one_sided_label_smoothing:  # Add noise only to ground truth labels

                noise = tf.random_normal(shape=tf.shape(input_labels), mean=0.0,
                                         stddev=tf.cast(0.3, tf.float32) * tf.exp(
                                             -tf.cast(0.004, tf.float32) * epoch_approx))

                disc_input_labels = tf.cond(is_training,
                                            lambda: tf.clip_by_value(tf.add(input_labels, noise),0,1),
                                            lambda: input_labels)

                disc_input_generated = generator_outputs

            elif params.instance_noise:

                #  Instance Noise: Add noise to input_labels and generator_outputs
                #  (https://www.inference.vc/instance-noise-a-trick-for-stabilising-gan-training/

                noise1 = tf.random_normal(shape=tf.shape(input_labels), mean=0.0,
                                         stddev=0.1 * tf.exp(
                                             -0.004 * epoch_approx))
                noise2 = tf.random_normal(shape=tf.shape(generator_outputs), mean=0.0,
                                         stddev=0.1 * tf.exp(
                                             -0.004 * epoch_approx))

                disc_input_labels, disc_input_generated = tf.cond(is_training,
                                                                  lambda: (tf.add(input_labels, noise1),
                                                                           tf.add(generator_outputs, noise2)),
                                                                  lambda: (input_labels, generator_outputs))
            else:

                disc_input_labels = input_labels
                disc_input_generated = generator_output

            if "multiscale" in params.discriminator_model:  # Special handling for using the multiscale discriminator.
                with tf.name_scope("discriminator_fake"):
                    with tf.variable_scope("discriminator"):
                        disc_outputs_fake, disc_logits_fake = discriminator_fn(disc_input_generated, is_training, params)
                        prediction_fake = disc_outputs_fake[0]
                with tf.name_scope("discriminator_real"):
                    with tf.variable_scope("discriminator", reuse=True):
                        disc_outputs_real, disc_logits_real = discriminator_fn(disc_input_labels, is_training, params)
                        prediction_real = disc_outputs_real[0]
            else:
                with tf.name_scope("discriminator_fake"):
                    with tf.variable_scope("discriminator"):
                        prediction_fake, prediction_fake_logits = discriminator_fn(disc_input_generated, is_training, params)
                with tf.name_scope("discriminator_real"):
                    with tf.variable_scope("discriminator", reuse=True):
                        prediction_real, prediction_real_logits = discriminator_fn(disc_input_labels, is_training, params)

    with tf.name_scope("losses"):

        with tf.name_scope("generator_loss"):

            gen_loss_mce = tf.losses.sparse_softmax_cross_entropy(logits=generator_outputs_logits, labels=labels,
                                                                  reduction=tf.losses.Reduction.SUM_OVER_BATCH_SIZE)
            gen_loss_L1 = tf.losses.absolute_difference(input_labels, generator_outputs,
                                                        reduction=tf.losses.Reduction.SUM_OVER_BATCH_SIZE)

            if "multiscale" in params.discriminator_model:
                gen_loss_adv = tf.Variable(initial_value=0.0, dtype=tf.float32, trainable=False)

                for t in disc_logits_fake:
                    loss = tf.reduce_mean(
                            tf.nn.sigmoid_cross_entropy_with_logits(labels=tf.ones_like(t), logits=t))
                    gen_loss_adv = tf.add(gen_loss_adv, loss)
                gen_loss_adv = gen_loss_adv/len(disc_logits_fake)
            else:
                gen_loss_adv = tf.reduce_mean(
                    tf.nn.sigmoid_cross_entropy_with_logits(labels=tf.ones_like(prediction_fake_logits),
                                                            logits=prediction_fake_logits))

            gen_loss = params.weight_mce * gen_loss_mce + \
                       params.weight_l1 * gen_loss_L1 + \
                       params.weight_adv * gen_loss_adv + tf.losses.get_regularization_loss()
            model.gen_loss = gen_loss

        with tf.name_scope("discriminator_loss"):

            # Flip labels?
            flip_labels = tf.logical_and(is_training,
                                         tf.less(tf.random_uniform(shape=[]), params.flip_labels_probability))

            def disc_labels(x, flip_labels):
                """ Returns the ground truth labels for the discriminator, 1=real, 0=fake """
                """ Implements Label Flipping, i.e. if flip_labels = True, the labels are inverted """

                real, fake = tf.cond(flip_labels,
                                     lambda: (tf.zeros_like(x), tf.ones_like(x)),
                                     lambda: (tf.ones_like(x), tf.zeros_like(x)))
                return real, fake

            if "multiscale" in params.discriminator_model:

                disc_loss_fake = tf.Variable(initial_value=0.0, dtype=tf.float32, trainable=False)
                disc_loss_real = tf.Variable(initial_value=0.0, dtype=tf.float32, trainable=False)

                for tfake, treal in zip(disc_logits_fake, disc_logits_real):

                    disc_labels_real, disc_labels_fake = disc_labels(tfake, flip_labels)
                    disc_loss_fake = tf.add(disc_loss_fake,
                                                  tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(
                                                      labels=disc_labels_fake,
                                                      logits=tfake
                                                  )))
                    disc_loss_real = tf.math.add(disc_loss_real,
                                                  tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(
                                                      labels=disc_labels_real,
                                                      logits=treal
                                                  )))
                disc_loss_real = disc_loss_real / len(disc_logits_real)
                disc_loss_fake = disc_loss_real / len(disc_logits_fake)

            else:
                disc_labels_real, disc_labels_fake = disc_labels(prediction_fake_logits, flip_labels)
                disc_loss_fake = tf.reduce_mean(
                    tf.nn.sigmoid_cross_entropy_with_logits(labels=disc_labels_fake,
                                                            logits=prediction_fake_logits))

                disc_loss_real = tf.reduce_mean(
                    tf.nn.sigmoid_cross_entropy_with_logits(labels=disc_labels_real,
                                                            logits=prediction_real_logits))

            disc_loss = params.weight_adv * (disc_loss_real + disc_loss_fake)

    with tf.name_scope("train_ops"):

        update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)

        with tf.device(params.gpu_disc):
            with tf.name_scope("discriminator"):
                disc_train_vars = tf.trainable_variables(scope="discriminator")
                disc_optimizer = tf.train.AdamOptimizer(learning_rate=params.lr_disc)
                disc_grads_vars = disc_optimizer.compute_gradients(loss=disc_loss, var_list=disc_train_vars)
                train_disc_op = disc_optimizer.apply_gradients(grads_and_vars=disc_grads_vars)

        with tf.name_scope("generator"):
            gen_train_vars = tf.trainable_variables(scope="generator")
            gen_optimizer = tf.train.AdamOptimizer(learning_rate=params.lr_gen)

            with tf.control_dependencies(update_ops):
                with tf.control_dependencies([train_disc_op]):
                    train_op = gen_optimizer.minimize(loss=gen_loss, var_list=gen_train_vars,
                                                      global_step=global_step, colocate_gradients_with_ops=True)
                    model.train_op = train_op

    with tf.name_scope("metrics"):

        with tf.variable_scope("metrics"):

            metrics = {
                'accuracy': tf.metrics.accuracy(labels=labels, predictions=predictions),
                'mean_per_class_accuracy': tf.metrics.mean_per_class_accuracy(labels=labels,
                                                                              predictions=predictions, num_classes=3),
                'mean_iou': tf.metrics.mean_iou(labels=labels, predictions=predictions, num_classes=3),
                'loss_generator_mce': tf.metrics.mean(gen_loss_mce),
                'loss_generator_adv': tf.metrics.mean(gen_loss_adv),
                'loss_discriminator': tf.metrics.mean(disc_loss/params.weight_adv),
                'loss_generator_l1': tf.metrics.mean(gen_loss_L1),
                'discriminator_average_prediction_fake': tf.metrics.mean(tf.reduce_mean(prediction_fake)),
                'discriminator_average_prediction_real': tf.metrics.mean(tf.reduce_mean(prediction_real)),
                'discriminator_max_prediction_fake': tf.metrics.mean(tf.reduce_max(prediction_fake)),
                'discriminator_min_prediction_fake': tf.metrics.mean(tf.reduce_min(prediction_fake))
            }

        update_metrics_op = tf.group(*[op for _, op in metrics.values()])
        model.update_metrics_op = update_metrics_op

        metric_variables = tf.get_collection(tf.GraphKeys.LOCAL_VARIABLES, scope="metrics")
        metrics_values = {k: v[0] for k, v in metrics.items()}
        model.metrics_values = metrics_values

        metrics_init_op = tf.variables_initializer(metric_variables)
        model.metrics_init_op = metrics_init_op

        for k, v in metrics_values.items():
            if "loss" in k:
                tf.summary.scalar(k, v, family="losses", collections=["epoch"])
            elif "discriminator" in k:
                tf.summary.scalar(k, v, family="discriminator", collections=["epoch"])
            else:
                tf.summary.scalar(k, v, family="metrics", collections=["epoch"])
        summary_op = tf.summary.merge_all(key="epoch")

        for grad, var in disc_grads_vars:
            tf.summary.histogram(var.op.name + "/gradients", grad, collections=["step"], family="discriminator_grads")

        tf.summary.histogram("discriminator_predictions_fake", prediction_fake, collections=["step"], family="discriminator")
        tf.summary.histogram("discriminator_predictions_real", prediction_real, collections=["step"], family="discriminator")
        step_summary_op = tf.summary.merge_all(key="step")
        model.step_summary_op = step_summary_op
        model.summary_op = summary_op
        log.info("Model successfully built")

        return model


def train(params, sess, model, writer, data_init_op, batchsize):
    """ Train for one epoch"""
    sess.run(data_init_op)
    sess.run(model.metrics_init_op)

    steps_per_epoch = params.train_data_size/batchsize
    pbar = tqdm(total=steps_per_epoch, position=1, leave=False)
    pbar.set_description("Train...")
    stepcounter = 0
    while True:

        try:
            fetches = {'update_metrics': model.update_metrics_op,
                       'train_op': model.train_op,
                       'step': model.global_step}

            if stepcounter % 12 == 0:
                fetches['step_summary'] = model.step_summary_op

            results = sess.run(fetches, feed_dict={model.is_training: True})

            if stepcounter % 12 == 0:
                writer.add_summary(results["step_summary"], global_step=results["step"])
            stepcounter = stepcounter+1
            pbar.update()

        except tf.errors.OutOfRangeError:
            pbar.close()
            fetches = [model.summary_op, model.metrics_values, model.global_step]

            summary, metrics_values, step = sess.run(fetches, feed_dict={model.is_training: False})
            metrics_string = " ; ".join("{}: {:05.3f}".format(k, v) for k, v in metrics_values.items())
            log.info("- Train metrics: " + metrics_string)

            writer.add_summary(summary, global_step = step)
            break


def evaluate(params, sess, model, writer, data_init_op):
    """ Evaluate the model for one epoch """
    sess.run(data_init_op)
    sess.run(model.metrics_init_op)

    steps_per_epoch = params.eval_data_size
    pbar = tqdm(total=steps_per_epoch, position=1, leave=False)
    pbar.set_description("Eval...")
    while True:

        try:

            sess.run(model.update_metrics_op, feed_dict={model.is_training: False}) # Update metrics, no training
            pbar.update()

        except tf.errors.OutOfRangeError:
            pbar.close()
            fetches = {'metrics_values': model.metrics_values,
                       'summary': model.summary_op,
                       'step': model.global_step}
            results = sess.run(fetches, feed_dict={model.is_training: False})

            metrics_string = " ; ".join("{}: {:05.3f}".format(k, v) for k, v in results["metrics_values"].items())
            log.info("- Evaluation metrics: " + metrics_string)

            writer.add_summary(results["summary"], global_step=results["step"])

            return results["metrics_values"]


def export(params, sess, model, data_init_op, model_dir, saveraw=False):
    """
    Exports generated label maps to pngs

    saveraw: This also saves (gt and generated) labels and images to npy files for easier handling.
    """
    

    sess.run(data_init_op)
    sess.run(model.metrics_init_op)

    fetches = {'input_images': model.input_images,
               'input_labels': model.input_labels,
               'generated_labels': model.generated_labels,
               'metrics_update': model.update_metrics_op,
               }
    counter = 0
    savepath = os.path.join(model_dir, "results")
    os.makedirs(savepath, exist_ok=True)
    savepath_json = os.path.join(savepath, 'npy')

    while True:

        try:

            results = sess.run(fetches, feed_dict={model.is_training: False})
            counter = counter + 1
            utils.plot_data_with_labels_v2(results["input_images"], results["input_labels"], results["generated_labels"],
                                        savepath=os.path.join(savepath, "plot{}.png".format(counter)))

            if saveraw:
                os.makedirs(os.path.join(savepath_json, "pat{}".format(counter)), exist_ok=True)
                np.save(os.path.join(savepath_json, "pat{}".format(counter), "images.npy"), results["input_images"])
                np.save(os.path.join(savepath_json, "pat{}".format(counter), "labels.npy"), results["input_labels"])
                np.save(os.path.join(savepath_json, "pat{}".format(counter), "pred.npy"), results["generated_labels"])

        except tf.errors.OutOfRangeError:
            metrics = sess.run(model.metrics_values, feed_dict={model.is_training: False})
            json_path = os.path.join(savepath, "metrics.json")
            utils.save_dict_to_json(metrics, json_path)
            break


def train_and_evaluate(params, model_dir, restore_from=None):
    """ Train and evaluate the model"""
    tf.set_random_seed(params.random_seed)

    with tf.name_scope("input_pipelines/"):
        with tf.name_scope("train_inputs"):
            train_data_iterator, train_data_init_op = input_fn(params.datadir, params.batch_size, params.buffer_size,
                                                               params.num_parallel_calls, params.inputsize,
                                                               params.shuffle, mode="train")
        with tf.name_scope("eval_inputs"):
            eval_data_iterator, eval_data_init_op = input_fn(params.datadir, batch_size=1, buffer_size=0,
                                                             num_parallel_calls=params.num_parallel_calls,
                                                             inputsize=params.inputsize, shuffle=False, mode="valid")

    model = build_model(params=params, train_input_iterator=train_data_iterator, eval_input_iterator=eval_data_iterator)

    best_saver = tf.train.Saver(max_to_keep=1)
    last_saver = tf.train.Saver(max_to_keep=2)

    best_eval_iou = 0.0
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    config.log_device_placement = False
    config.allow_soft_placement = True

    with tf.Session(config=config) as sess:
        begin_at_epoch = 0

        train_writer = tf.summary.FileWriter(os.path.join(model_dir, "train_summaries"), sess.graph)
        eval_writer = tf.summary.FileWriter(os.path.join(model_dir, "eval_summaries"), sess.graph)

        sess.run(tf.global_variables_initializer())

        variables_names = [v.name for v in tf.trainable_variables()]
        values = sess.run(variables_names)
        for k, v in zip(variables_names, values):
            print(k, v.shape)
        parameter_count = tf.reduce_sum([tf.reduce_prod(tf.shape(v)) for v in tf.trainable_variables()])
        print("Trainable Parameters: {}".format(sess.run(parameter_count)))
        # Restore weights if necessary
        if restore_from is not None:
            log.info("Restoring parameters from {}".format(restore_from))
            if os.path.isdir(restore_from):
                restore_from = tf.train.latest_checkpoint(restore_from)
                begin_at_epoch = int(restore_from.split('-')[-1])
                best_saver.restore(sess, restore_from)

        pbar_epoch = tqdm(total=params.num_epochs, initial=begin_at_epoch+1, desc="Total Progress", position=0)

        for epoch in range(begin_at_epoch, begin_at_epoch + params.num_epochs):

            log.info("Epoch {}/{}".format(epoch + 1, begin_at_epoch + params.num_epochs))

            train(params, sess, model, train_writer, train_data_init_op, params.batch_size)

            metrics = evaluate(params, sess, model, eval_writer, eval_data_init_op)

            last_save_path = os.path.join(model_dir, 'last_weights', 'after-epoch')
            last_saver.save(sess, last_save_path, global_step=epoch + 1)

            # Save model when best performance so far
            if metrics["mean_iou"] > best_eval_iou:
                best_save_path = os.path.join(model_dir, "best_weights", "after-epoch")
                best_save_path = best_saver.save(sess, best_save_path, global_step=epoch + 1)
                log.info("- Found new best performance, saving in {}".format(best_save_path))

                best_json_path = os.path.join(os.path.join(model_dir, "metrics_eval_best_weights.json"))
                utils.save_dict_to_json(metrics, best_json_path)
                best_eval_iou = metrics["mean_iou"]
            pbar_epoch.update()

        pbar_epoch.close()


def export_plots(params, model_dir, restore_from, datasrc="valid", saveraw=False):
    tf.set_random_seed(params.random_seed)

    with tf.name_scope("input_pipelines/"):
        with tf.name_scope("inputs"):
            data_iterator, data_init_op = input_fn(params.datadir, batch_size=1, buffer_size=1,
                                                   num_parallel_calls=params.num_parallel_calls,
                                                   inputsize=params.inputsize,
                                                   shuffle=False, mode=datasrc)

    model = build_model(params=params, train_input_iterator=data_iterator, eval_input_iterator=data_iterator)
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    best_saver = tf.train.Saver(max_to_keep=1)

    with tf.Session(config=config) as sess:
        sess.run(tf.global_variables_initializer())
        log.info("Restoring parameters from {}".format(restore_from))
        if os.path.isdir(restore_from):
            restore_from = tf.train.latest_checkpoint(restore_from)
            best_saver.restore(sess, restore_from)
        else:
            log.error("No valid checkpoint found")
            exit()
        log.info("Starting export")
        export(params=params, sess=sess, model=model, data_init_op=data_init_op, model_dir=model_dir, saveraw=saveraw)
        log.info("Export finished")

