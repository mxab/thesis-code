import numpy as np
import tensorflow as tf


def plot_data_with_labels(images, labels, labels_generated, savepath=None):

    """ Plots generated labels and ground truth overlaying inputs 
    Expects input format: (D,H,W,C)
    Kind of deprecated, use v2 below """

    import matplotlib
    matplotlib.use('Agg')
    import matplotlib.pyplot as plt
    import numpy as np

    def masked_image(image, condition=0):
        return np.ma.masked_where(image == condition, image)

    def plot_view(data_image, data_background, data_liver, data_spleen):
        plt.imshow(data_image, cmap='Greys')
        plt.imshow(masked_image(data_liver, 0), cmap='Set1', alpha=0.6)
        plt.imshow(masked_image(data_spleen, 0), cmap='tab10', alpha=0.6)
        plt.imshow(masked_image(data_background, 0), cmap='Pastel2', alpha=0.6)

    depth = np.argmin(np.sum(labels[..., 0], axis=(1, 2)))
    width = np.argmin(np.sum(labels[..., 0], axis=(0, 1)))
    height = np.argmin(np.sum(labels[..., 0], axis=(0, 2)))

    # 1. Ansicht: generated labels
    fig = plt.subplot(3,2,1)
    plot_view(images[depth, ..., 1], labels_generated[depth, ..., 0],
              labels_generated[depth, ..., 1], labels_generated[depth, ..., 2])
    fig.set_title("Generated Labels")

    # 1. Ansicht: ground truth
    fig = plt.subplot(3,2,2)
    plot_view(images[depth, ..., 1], labels[depth, ..., 0], labels[depth, ..., 1], labels[depth, ..., 2])
    fig.set_title("Ground Truth")

    # 2. Ansicht: generated labels
    fig = plt.subplot(3,2,3)
    plot_view(images[:, height, :, 1], labels_generated[:, height, :, 0],
              labels_generated[:, height, :, 1], labels_generated[:, height, :, 2])

    # 2. Ansicht: ground truth
    fig = plt.subplot(3,2,4)
    plot_view(images[:, height, :, 1], labels[:,height, :, 0], labels[:, height, :, 1], labels[:, height, :, 2])

    # 3. Ansicht: generated labels
    fig = plt.subplot(3,2,5)
    plot_view(images[..., width, 1], labels_generated[..., width, 0],
              labels_generated[..., width, 1], labels_generated[..., width, 2])

    # 3. Ansicht: ground truth
    fig = plt.subplot(3,2,6)
    plot_view(images[..., width, 1], labels[..., width, 0], labels[..., width, 1], labels[..., width, 2])

    if savepath is None:
        plt.show()
    else:
        plt.savefig(savepath)


def plot_data_with_labels_v2(images, labels, labels_generated, savepath):
    """ New Version. """
    import matplotlib
    matplotlib.use('Agg')
    from matplotlib import colors
    import matplotlib.pyplot as plt

    # Get rid of batch/channel dimension in case they still are there
    images = np.squeeze(images)
    labels = np.squeeze(labels)
    labels_generated = np.squeeze(labels_generated)

    miou = mean_iou(labels, labels_generated, 3)
    pciou = per_class_iou(labels, labels_generated, 3)
    print(len(pciou))
    description = 'IoU Background: {0:04.3f}\nIoU Liver: {1:04.3f}\nIoU Spleen: {2:04.3f}'.format(pciou[0], pciou[1], pciou[2])
    # Expects (D,H,W) for labels -> argmax
    # Find most interesting depth (least pixels==background)
    depth = np.sum(labels, axis=(1, 2))
    depth = np.argmax(depth)

    images = images[depth, ..., 0]
    # Mask background labels
    labels = np.ma.masked_equal(labels[depth, ...], 0)
    labels_generated = np.ma.masked_equal(labels_generated[depth, ...], 0)

    fig = plt.figure()
    cmap = colors.ListedColormap(['red', 'green'])
    bounds = [1, 2, 3]
    norm = colors.BoundaryNorm(bounds, cmap.N)

    ax1 = fig.add_subplot(1, 2, 1)
    ax1.imshow(images, cmap='gray')
    ax1.imshow(labels, cmap=cmap, norm=norm, alpha=0.6)
    ax1.set_title("Ground Truth")
    ax1.set_axis_off()

    ax2 = fig.add_subplot(1, 2, 2)
    ax2.imshow(images, cmap='gray')
    ax2.imshow(labels_generated, cmap=cmap, norm=norm, alpha=0.6)
    ax2.set_title("Generated Labels (mIoU: {:04.3f})".format(miou))
    ax2.set_axis_off()
    ax2.annotate(description,
        (0, 0), (0, -20), xycoords='axes fraction', textcoords='offset points', va='top')
    plt.savefig(savepath, bbox_inches='tight')
    plt.close(fig)


def confusion_matrix(labels, labels_generated, c):

    tp = np.sum(np.logical_and(labels == c, labels_generated == c), axis=(0,1,2)).astype("f")
    tn = np.sum(np.logical_and(labels != c, labels_generated != c), axis=(0,1,2)).astype("f")
    fp = np.sum(np.logical_and(labels_generated == c, labels != c), axis=(0, 1, 2)).astype("f")
    fn = np.sum(np.logical_and(labels_generated != c, labels == c), axis=(0, 1, 2)).astype("f")

    if tp+tn+fp+fn != np.size(labels):
        raise Exception
    print(tp, tn)
    return tp, tn, fp, fn


def accuracy(labels, labels_generated, c):

    total = np.size(labels)
    count = np.sum(labels == labels_generated)
    return count/total


def per_class_accuracy(labels, labels_generated, num_classes):

    pca = []

    for c in range(num_classes):
        tp, tn, fp, fn = confusion_matrix(labels, labels_generated, c)
        pca.append((tp+tn)/(tp+tn+fp+fn))
    return pca

def mean_accuracy(labels, labels_generated, num_classes):
    return np.mean(per_class_accuracy(labels, labels_generated, num_classes))

def per_class_iou(labels, labels_generated, num_classes):

    # True positive: labels_generated == 1 and labels == 1
    # False positive: labels_generated == 1 and labels == 0
    # true negative: labels_generated == 0 and labels == 0
    # false negative: labels_generated == 0 and labels == 1

    iou = []
    for c in range(num_classes):

        tp = np.sum(np.logical_and(labels == c, labels_generated == c), axis=(0,1,2)).astype("f")
        fp = np.sum(np.logical_and(labels_generated == c, labels != c), axis=(0,1,2)).astype("f")
        fn = np.sum(np.logical_and(labels_generated != c, labels == c), axis=(0, 1, 2)).astype("f")
        iou.append(tp/(tp+fp+fn))
    return iou


def mean_iou(labels, labels_generated, num_classes):
    return np.mean(per_class_iou(labels, labels_generated, num_classes))


def weighted_iou(labels, labels_generated, num_classes):

    weights = []
    num_elements = np.size(labels)
    for c in range(num_classes):
        weights.append(np.sum(labels==c).astype("f")/num_elements)

    return np.sum(np.multiply(weights, per_class_iou(labels, labels_generated, num_classes)))


def logits2labels(logits):

    am = np.argmax(logits, axis=-1)
    labels = np.zeros(shape=(160, 288, 288, 3))
    labels[(am == 0), 0] = 1
    labels[(am == 1), 1] = 1
    labels[(am == 2), 2] = 1
    return labels


def save_dict_to_json(d, json_path):
    """Saves dict of floats in json file
    Args:
        d: (dict) of float-castable values (np.float, int, float, etc.)
        json_path: (string) path to json file
    """
    with open(json_path, 'w') as f:
        # We need to convert the values to float for json (it doesn't accept np.array, np.float, )
        d = {k: float(v) for k, v in d.items()}
        json.dump(d, f, indent=4)

import json
import logging
from tqdm import tqdm


class Params():
    """Class that loads hyperparameters from a json file.
    Example:
    ```
    params = Params(json_path)
    print(params.learning_rate)
    params.learning_rate = 0.5  # change the value of learning_rate in params
    ```
    """

    def __init__(self, json_path):
        self.update(json_path)

    def save(self, json_path):
        """Saves parameters to json file"""
        with open(json_path, 'w') as f:
            json.dump(self.__dict__, f, indent=4)

    def update(self, json_path):
        """Loads parameters from json file"""
        with open(json_path) as f:
            params = json.load(f)
            self.__dict__.update(params)

    @property
    def dict(self):
        """Gives dict-like access to Params instance by `params.dict['learning_rate']`"""
        return self.__dict__


def set_logger(log_path):
    """Sets the logger to log info in terminal and file `log_path`.
    In general, it is useful to have a logger so that every output to the terminal is saved
    in a permanent file. Here we save it to `model_dir/train.log`.
    Example:
    ```
    logging.info("Starting training...")
    ```
    Args:
        log_path: (string) where to log
    """
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)

    logger_tf = logging.getLogger("tensorflow")
    logger.setLevel(logging.INFO)


    class TqdmHandler(logging.StreamHandler):
        def __init__(self):
            logging.StreamHandler.__init__(self)

        def emit(self, record):
            msg = self.format(record)
            tqdm.write(msg)

    if not logger.handlers:
        # Logging to a file
        file_handler = logging.FileHandler(log_path)
        file_handler.setFormatter(logging.Formatter('%(asctime)s:%(levelname)s: %(message)s'))
        logger.addHandler(file_handler)
        logger_tf.addHandler(file_handler)

        tqdm_handler = TqdmHandler()
        tqdm_handler.setFormatter(logging.Formatter('%(message)s'))
        logger.addHandler(tqdm_handler)
        logger_tf.addHandler(tqdm_handler)

