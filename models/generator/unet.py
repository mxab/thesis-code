"""
The generator models are defined here (all models, not just unet, disregard the filename).
"""
import tensorflow as tf


def encode_block(x, filters, strides=None, batchnorm=True, is_training=True, bottleneck=False,
                 dropout=None, name=None, reg=None):

    if strides is None:
        strides = [2, 2, 2]

    with tf.name_scope(name):
        net = tf.layers.conv3d(x, filters=filters, kernel_size=[4, 4, 4], strides=strides, padding='same',
                               kernel_initializer=tf.glorot_normal_initializer(),
                               kernel_regularizer=reg)

        net = tf.nn.leaky_relu(net)

        if batchnorm:
            net = tf.layers.batch_normalization(net, training=is_training)

        if dropout is not None:
            net = tf.layers.dropout(net, rate=dropout, training=is_training)

        if not bottleneck:
            skip = net
        else:
            skip = None

    return net, skip


def encode_block_dil(x, filters, dilation=None, dilation_depth=None, batchnorm=True,
                     is_training=True, bottleneck=False, dropout=None, name=None):

    if dilation is None:
        dilation = 2

    with tf.name_scope(name):
        with tf.variable_scope(name):
            net = tf.layers.conv3d(x, filters=filters[0], kernel_size=(4, 4, 4),
                                   dilation_rate=(dilation_depth[0], dilation[0], dilation[0]), padding='SAME')
            net = tf.layers.conv3d(net, filters=filters[1], kernel_size=(4, 4, 4),
                                   dilation_rate=(dilation_depth[0], dilation[1], dilation[1]), padding='SAME')
            net = tf.layers.conv3d(net, filters=filters[1], kernel_size=(4, 4, 4),
                                   dilation_rate=(dilation_depth[2], dilation[2], dilation[2]), padding='SAME')
            net = tf.nn.conv3d(net, filter=tf.ones(shape=[1, 1, 1, filters[1], filters[1]]), strides=[1, 2, 2, 2, 1],
                               padding="SAME")
            net = tf.nn.leaky_relu(net)

            if batchnorm:
                net = tf.layers.batch_normalization(net, training=is_training)

            if dropout is not None:
                net = tf.layers.dropout(net, rate=dropout, training=is_training)

    if not bottleneck:
        return net, net
    else:
        return net, None


def decode_block(x, x_skip, filters, batchnorm=True, is_training=True, relu=True, softmax=False,
                 dropout=None, name=None, reg=None):
    with tf.name_scope(name):
        net = tf.layers.conv3d_transpose(x, filters=filters, kernel_size=(4, 4, 4), strides=(2, 2, 2), padding='SAME',
                                         kernel_initializer=tf.glorot_normal_initializer(),
                                         kernel_regularizer=reg)

        if relu:
            net = tf.nn.relu(net)  # No Leaky ReLu in decoder (consistent with pix2pix/SegAN)

        if softmax:
            net = tf.nn.softmax(net)

        if batchnorm:
            net = tf.layers.batch_normalization(net, training=is_training)

        if dropout is not None:
            net = tf.layers.dropout(net, rate=dropout, training=is_training)

        if x_skip is not None:
            net = tf.concat([net, x_skip], axis=-1)

    return net

def dilated_block(x, filters, batchnorm=True, is_training=True, dilations=None, activation=True, dropout=None, name=None):
    """ Dilated Convolutions in 3D are weird in TF (at least in 1.10). The only way to get them to work semi-properly
    is to use tf.nn.convolution, which still has the excessive memory issue.
    See here: https://github.com/tensorflow/tensorflow/issues/8431
    """

    if dilations is None:
        dilations = 1

    with tf.variable_scope(name):

        kernel = tf.get_variable(name="kernel", shape=[4, 4, 4, x.get_shape()[-1], filters], trainable=True)
        bias = tf.get_variable(name="bias", shape=[filters], initializer=tf.zeros_initializer(), trainable=True)

        net = tf.nn.convolution(input=x, filter=kernel, padding="SAME", dilation_rate=[dilations, dilations, dilations])
        net = net + bias

        if activation:
            net = tf.nn.leaky_relu(net)

        if batchnorm:
            net = tf.layers.batch_normalization(net, training=is_training)

        if dropout is not None:
            net = tf.layers.dropout(net, dropout, training=is_training)

    return net


def unet(x, output_channels=3, batchnorm=True, is_training=True, reg=None):

    with tf.name_scope('encoder'):
        enc = [None]*3
        net, enc[0] = encode_block(x, filters=32, batchnorm=False, is_training=is_training, dropout=0.5, name='layer1', reg=reg)  # 32 64
        net, enc[1] = encode_block(net, filters=64, batchnorm=batchnorm, is_training=is_training, dropout=0.5, name='layer2', reg=reg)  # 64 128
        net, enc[2] = encode_block(net, filters=128, batchnorm=batchnorm, is_training=is_training, dropout=0.25, name='layer3', reg=reg)  # 128 256

    with tf.name_scope('bottleneck'):
        net, _ = encode_block(net, filters=256, batchnorm=batchnorm, is_training=is_training,
                              bottleneck=True, name='layer', reg=reg)  # 256 512

    with tf.name_scope('decoder'):
        net = decode_block(net, enc[2], filters=128, batchnorm=batchnorm, is_training=is_training, dropout=0.5, name='layer1', reg=reg)  # 256 256
        net = decode_block(net, enc[1], filters=64, batchnorm=batchnorm, is_training=is_training, dropout=0.5, name='layer2', reg=reg)  # 128 128
        net = decode_block(net, enc[0], filters=32, batchnorm=batchnorm, is_training=is_training, name='layer3', reg=reg)  # 64 64

        logits = decode_block(net, None, filters=output_channels, batchnorm=False, relu=False, softmax=False, name='layer4', reg=reg)
        outputs = tf.nn.softmax(logits)

    return outputs, logits


def dilated_fcn(x, is_training=True):

    net = dilated_block(x, filters=16, batchnorm=False, is_training=is_training, dropout=0.5, dilations=1, name='layer1')
    net = dilated_block(net, filters=16, dilations=1, is_training=is_training, dropout=0.5, name='layer2')
    net = dilated_block(net, filters=16, dilations=2, is_training=is_training, dropout=0.5, name='layer3')
    net = dilated_block(net, filters=16, dilations=4, is_training=is_training, dropout=0.5, name='layer4')
    net = dilated_block(net, filters=16, dilations=8, is_training=is_training, dropout=0.5, name='layer5')
    net = dilated_block(net, filters=16, dilations=16, is_training=is_training, dropout=0.5, name='layer6')
    net = dilated_block(net, filters=16, dilations=1, is_training=is_training, dropout=0.5, name='layer7')

    logits = dilated_block(net, filters=3, batchnorm=False, is_training=is_training, activation=False, name='output')
    outputs = tf.nn.softmax(logits)

    return outputs, logits
