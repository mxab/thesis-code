"""
Wrapper Function. Returns the requested generator model from unet.py.
"""
def generator_fn(x, is_training, params, reg=None):

    if params.generator_model == "unet":

        from .unet import unet

        if reg is not None:
            return unet(x, output_channels=3, batchnorm=params.batchnorm, is_training=is_training, reg=reg)
        else:
            return unet(x, output_channels=3, batchnorm=params.batchnorm, is_training=is_training)

    if params.generator_model == "unet2":

        from .unet import unet2

        if reg is not None:
            return unet2(x, output_channels=3, batchnorm=params.batchnorm, is_training=is_training, reg=reg)
        else:
            return unet2(x, output_channels=3, batchnorm=params.batchnorm, is_training=is_training)

    if params.generator_model == "unet_dilated":

        from .unet import unet_dilated

        return unet_dilated(x, output_channels=3, batchnorm=params.batchnorm, is_training=is_training,
                            dilations=params.gen_dilations)

    if params.generator_model == "dilated_fcn":

        from .unet import dilated_fcn

        return dilated_fcn(x, is_training=is_training)
