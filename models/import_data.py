""" This loads the training data into a tf.data-pipeline. Requires Marc's medio. """
import tensorflow as tf
import os
import json
from medio import convert_tf
from medio import parse_tf


def downsize(images, labels, size):

    # This currently downsamples only height and width of the volume and crops the depth
    # Expects dcm.shape = (D,H,W,C)
    # Expects size = [D,H,W]

    depth = size[0]
    height = size[1]
    width = size[2]

    # Crop depth
    startd = tf.shape(images)[0] // 2 - depth // 2
    startd = tf.cast(startd, tf.int32)

    images = images[startd:startd + depth, ...]
    labels = labels[startd:startd+depth, ...]
    images.set_shape([depth, 288, 288, 2])  # Set static shapes
    labels.set_shape([depth, 288, 288, 2])  # Background gets added later

    #  Resize height and width
    images = tf.image.resize_nearest_neighbor(images=images, size=[height, width], align_corners=True, name='resize_inputs')
    labels = tf.image.resize_nearest_neighbor(images=labels, size=[height, width], align_corners=True, name='resize_labels')

    return images, labels


def create_background(images, labels):

    #  Adds a background channel to labels, s.t. 0->background, 1->liver, 2->spleen
    background_channel = tf.zeros_like(labels[..., 0])
    background_mask = tf.logical_and(tf.equal(labels[..., 0], 0), tf.equal(labels[..., 1], 0))
    background_channel = tf.where(background_mask, x=tf.ones_like(labels[..., 0]), y=background_channel)
    background_channel = tf.expand_dims(background_channel, axis=-1)
    labels = tf.concat([background_channel, labels], axis=-1)
    return images, labels


def input_fn(datadir, batch_size, buffer_size, num_parallel_calls, inputsize=[80, 144, 144],
             shuffle=True, mode="train", legacy=False):

    def _map_data(a, b, c, d):

        # All map_functions inside one for possible performance reasons

        a = convert_tf.parse_function(a)
        b = convert_tf.parse_function(b)
        c = convert_tf.parse_function(c)
        d = convert_tf.parse_function(d)

        if legacy:
            a = tf.image.per_image_standardization(a)
            b = tf.image.per_image_standardization(b)
        else:
            a = tf.cast(a, tf.float64)
            b = tf.cast(b, tf.float64)
            moments = tf.nn.moments(a, axes=[0, 1, 2])
            a = (a - moments[0]) / tf.sqrt(moments[1])

            moments = tf.nn.moments(b, axes=[0, 1, 2])
            b = (b - moments[0]) / tf.sqrt(moments[1])

        a = tf.slice(a, [0, 0, 0], [288, 160, 288])
        b = tf.slice(b, [0, 0, 0], [288, 160, 288])
        c = tf.slice(c, [0, 0, 0], [288, 160, 288])
        d = tf.slice(d, [0, 0, 0], [288, 160, 288])

        # Stack and transpose s.t. images, labels are in (D,H,W,C) format
        img = tf.transpose(tf.stack([a, b], axis=3), perm=[1, 0, 2, 3])
        lbl = tf.transpose(tf.stack([c, d], axis=3), perm=[1, 0, 2, 3])

        # Check the inputs first
        if inputsize is not None:
            img, lbl = downsize(img, lbl, size=inputsize)
        img, lbl = create_background(img, lbl)

        img = tf.cast(img, tf.float32)
        lbl = tf.cast(lbl, tf.float32)
        return img, lbl

    with tf.device('/cpu:*'):  # Run Pipeline on any CPU available

        with open(os.path.join(datadir, mode+"_data.json"), "r") as f:
            data = json.load(f)
            images_water = data["images_water"]
            images_fat = data["images_fat"]
            labels_liver = data["labels_liver"]
            labels_spleen = data["labels_spleen"]

        dataset_image_water = tf.data.TFRecordDataset(images_water)
        dataset_image_fat = tf.data.TFRecordDataset(images_fat)
        dataset_labels_liver = tf.data.TFRecordDataset(labels_liver)
        dataset_labels_spleen = tf.data.TFRecordDataset(labels_spleen)
        dataset = tf.data.Dataset.zip((dataset_image_water, dataset_image_fat, dataset_labels_liver,
                                       dataset_labels_spleen))
        if shuffle:
            dataset = dataset.shuffle(buffer_size=140)

        dataset = dataset.map(map_func=_map_data, num_parallel_calls=num_parallel_calls)

        dataset = dataset.batch(batch_size=batch_size, drop_remainder=True)

        if buffer_size > 0:
            dataset = dataset.prefetch(buffer_size=buffer_size)

        iterator = dataset.make_initializable_iterator()
        iterator_init_op = iterator.make_initializer(dataset=dataset, name='iterator_init_op')

        return iterator, iterator_init_op
