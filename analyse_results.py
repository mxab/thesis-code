#!/opt/python-3.6.5/bin/python3.6
import numpy as np
import os
import argparse
from models.utils import per_class_iou
import matplotlib
matplotlib.use('tkagg')
import matplotlib.pyplot as plt

parser = argparse.ArgumentParser()
parser.add_argument('--model_dir', default='experiments/test',
                    help="Experiment directory containing params.json")

args = parser.parse_args()

resdir = os.path.join(args.model_dir, "results", "npy")

def get_organ_size(labels):

    retval = []
    num_pixels = np.size(labels)
    for i in [0, 1, 2]:
        pixels_in_class = labels[labels[...,:] == i]
        num_pixels_in_class = np.size(pixels_in_class)
        retval.append(num_pixels_in_class/num_pixels)
    return retval

patients = [f.name for f in os.scandir(resdir) if f.is_dir()]

liver_sizes = []
spleen_sizes = []
liver_iou = []
spleen_iou = []
for patient in patients:

    labels = np.load(os.path.join(resdir, patient, "labels.npy"))
    labels = np.squeeze(labels)
    gen_labels = np.load(os.path.join(resdir, patient, "pred.npy"))
    gen_labels = np.squeeze(gen_labels)
    print(np.max(gen_labels))


    print(patient)
    organ_sizes = get_organ_size(labels)[1:]
    liver_sizes.append(organ_sizes[0])
    spleen_sizes.append(organ_sizes[1])
    pciou = per_class_iou(labels=labels, labels_generated=gen_labels, num_classes=3)
    print(pciou)
    liver_iou.append(pciou[1])
    spleen_iou.append(pciou[2])

print(np.mean(liver_iou))
print(np.mean(spleen_iou))
plt.plot(spleen_sizes/np.max(spleen_sizes), spleen_iou, 'k.')
plt.plot(liver_sizes/np.max(liver_sizes), liver_iou, 'b.')

z = np.polyfit(spleen_sizes/np.max(spleen_sizes), spleen_iou, 1)
p = np.poly1d(z)

plt.plot(spleen_sizes/np.max(spleen_sizes), p(spleen_sizes/np.max(spleen_sizes)), "k--")

z = np.polyfit(liver_sizes/np.max(liver_sizes), liver_iou, 1)
p = np.poly1d(z)

plt.plot(liver_sizes/np.max(liver_sizes), p(liver_sizes/np.max(liver_sizes)), "b--")

plt.xlabel("Normalized Organ Size")
plt.ylabel("Jaccard score")
plt.show()

