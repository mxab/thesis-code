import tensorflow as tf
from models.generator.unet import unet
import matplotlib
matplotlib.use('qt4agg')
from matplotlib import pyplot as plt
import numpy as np

data = 0.01*tf.ones(shape=[1,80,144,144,2], dtype=tf.float32)
#data = tf.get_variable(name='data', shape=[1,80,144,144,2], dtype=tf.float32)
gen_outputs, gen_logits = unet(data)
koi = tf.get_collection("koi")[0]
print(koi)
koi = koi[0,5,9,9,:]
grads = tf.gradients(koi, data)
grads = grads[0][0,:,:,:,:]
grads = tf.abs(grads)
with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())
    gr = sess.run(grads)
    print(gr.shape)
    print(np.max(gr))
    sess.close()
    gr=np.sum(gr[:, :, :], axis=(0,-1))
    gr = np.ma.masked_where(gr<=0.1*np.max(gr), gr)
    image = np.load('/home/max/Schreibtisch/patient1/images.npy')
    plt.imshow(np.rot90(image[48,:,:,0]), cmap='gray')
    im=plt.imshow(np.rot90(gr/np.max(gr)), cmap='jet', alpha=0.8)
    plt.colorbar(im)
    plt.show()

