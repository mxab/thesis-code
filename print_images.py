import tensorflow as tf
import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot as plt
from matplotlib import colors
from models.import_data import input_fn
import numpy as np
import os

datadir = "/home/d1266/no_backup/d1266"
num_patients = 1
data_iterator, data_init_op = input_fn(datadir, batch_size=1, buffer_size=0,
                                       num_parallel_calls=1,
                                       inputsize=None,
                                       shuffle=False, mode="train")
os.environ["CUDA_VISIBLE_DEVICES"] = ""

with tf.Session() as sess:
    sess.run(data_init_op)

    for i in range(num_patients):

        img, lbl = data_iterator.get_next()

        img, lbl = sess.run([img, lbl])

        img = np.squeeze(img)
        lbl = np.squeeze(lbl)
        lbl = np.argmax(lbl, axis=-1)
        print(img.shape)
        print(lbl.shape)
        lbl = np.ma.masked_where(lbl == 0, lbl)

        fig = plt.figure()
        cmap = colors.ListedColormap(['red', 'green'])
        bounds = [1, 2, 3]
        norm = colors.BoundaryNorm(bounds, cmap.N)
        exportdir = os.path.join(datadir, "slices", "patient{}".format(i), "view1")
        os.makedirs(exportdir, exist_ok=True)

        for i2 in range(0, 160, 5):

            plt.imshow(np.rot90(img[i2, ..., 0]), cmap="gray")
            plt.imshow(np.rot90(lbl[i2, ...]), cmap=cmap, norm=norm, alpha=0.3)

            plt.gca().set_axis_off()
            plt.axis('off')
            plt.gca().xaxis.set_major_locator(plt.NullLocator())
            plt.gca().yaxis.set_major_locator(plt.NullLocator())
            plt.subplots_adjust(top=1, bottom=0, right=1, left=0,
                                hspace=0, wspace=0)

            plt.savefig(os.path.join(exportdir, "slice{}.png".format(i2)), bbox_inches="tight", pad_inches=0.0)
            plt.close()







