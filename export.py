#!/opt/python-3.6.5/bin/python3.6

""" Export results from a finished run """

import argparse
import os
from models.utils import Params, set_logger
from models.training import export_plots
import GPUtil

parser = argparse.ArgumentParser()
parser.add_argument('--model_dir', default='experiments/test',
                    help="Experiment directory containing params.json")

parser.add_argument('--restore_from', default=None,
                    help="Optional, directory or file containing weights to reload before training")

parser.add_argument('--force_gpu', help="Optional, forces usage of specific GPU(s)", default=None)
parser.add_argument('--datasrc', help="Optional, use a specific dataset. Defaults to validation.", default="valid")
parser.add_argument('--saveraw', help="Optional, if true saves the results as numpy files for further use", default=False)

if __name__ == '__main__':

    args = parser.parse_args()
    json_path = os.path.join(args.model_dir, 'params.json')
    assert os.path.isfile(json_path), "No json configuration file found at {}".format(json_path)
    params = Params(json_path)

    #  Determine which GPU is available
    availableGPUs = GPUtil.getAvailable(order='memory', limit=2, maxLoad=0.1, maxMemory=0.05)

    if args.force_gpu is not None:
        availableGPUs = args.force_gpu

    assert len(availableGPUs) >= params.req_num_gpus, "Not enough GPUs available, aborting"

    os.environ["CUDA_VISIBLE_DEVICES"] = str(availableGPUs[:params.req_num_gpus]).strip('[]')
    os.environ["LD_LIBRARY_PATH"] = "/usr/local/cuda-9.0-cudnn-7.1.2/extras/CUPTI/lib64/:$LD_LIBRARY_PATH"

    model_dir_has_best_weights = os.path.isfile(os.path.join(args.model_dir, "best_weights", "checkpoint")) # True
    no_best_weights = not model_dir_has_best_weights and args.restore_from is None # False and True => False
    assert not no_best_weights, "No Weights found in model_dir, aborting " # True

    set_logger(os.path.join(args.model_dir, 'train.log'))

    export_plots(params=params, model_dir=args.model_dir, restore_from=os.path.join(args.model_dir, "best_weights"),
                 datasrc=args.datasrc, saveraw=args.saveraw)
