#!/opt/python-3.6.5/bin/python3.6
import os
import subprocess
import GPUtil
import json
from sender import Mail, Message, Attachment


PID = os.getpid()
path = '/home/d1266/no_backup/d1266/'
PIDFILE = '/home/d1266/no_backup/d1266/RUNNING.pid'
expdir = '/home/d1266/no_backup/d1266/experiments/'


def send_mail(subject, message, attachment_file):

    fromaddr = "xxx@yyy.zz"
    toaddr = "xxx@yyy.zz"

    msg = Message(subject, fromaddr=fromaddr, to=toaddr, body=message)

    if attachment_file is not None:
        for at in attachment_file:
            with open(os.path.abspath(at)) as f3:
                attachment = Attachment(str(os.path.basename(at)), "application/json", f3.read())

            msg.attach(attachment)

    mail = Mail("127.0.0.1", port=587, use_ssl=False, username="", password="",
                use_tls=True)

    mail.send(msg)


def can_it_run():

    if os.path.isfile(PIDFILE):
        return False

    if not os.path.isfile(os.path.join(path,'scheduled_jobs.json')):
        return False

    availableGPUs = GPUtil.getAvailable(order='memory', limit=2, maxLoad=0.1, maxMemory=0.05)

    if len(availableGPUs) < 1:
        return False
    print("Can run")
    return True


def get_next_job():

    with open(os.path.join(path, 'scheduled_jobs.json'),"r") as fn2:
        res = json.load(fn2)
        jobs = res["jobs"]

    availableGPUs = GPUtil.getAvailable(order='memory', limit=2, maxLoad=0.1, maxMemory=0.05)
    for idx, jobc in enumerate(jobs):
        print(jobc)
        with open(os.path.join(expdir, jobc,"params.json"), "r") as f2:
            params = json.load(f2)

        if params["req_num_gpus"] <= len(availableGPUs): # Run this job
            jobs.pop(idx)
            if len(jobs) == 0:
                os.remove(os.path.join(path, 'scheduled_jobs.json'))
            else:
                with open(os.path.join(path, 'scheduled_jobs.json'), "w") as f2:
                    writedata=dict()
                    writedata["jobs"] = jobs
                    json.dump(writedata, f2)

            return jobc

    return None





def run(job):
    job_dir = os.path.join(expdir, job)
    # Write lock file containing process PID
    with open(PIDFILE, 'w') as f2:
        f2.write(str(PID))
    try:
        send_mail("Job gestartet", "Training gestartet\n"+str(os.path.basename(job_dir)), None)
    except:
        pass

    try:

        subprocess.run([os.path.join(path, "code", "train.py"), "--model_dir", job_dir], check=True)

    except Exception:
        # Irgendwas ist passiert.
        try:
            send_mail("Job fehlgeschlagen", "Subprocess exception.\n"+str(os.path.basename(job_dir)), None)
        except:
            pass

    finally:
        # removing lock file upon script execution
        os.remove(PIDFILE)
        try:
            send_mail("Job erfolgreich beendet", "Modell "+str(os.path.basename(job_dir))+" erfolgreich trainiert. Logs im Anhang.",
                      [os.path.join(job_dir, "train.log"), os.path.join(job_dir, "metrics_eval_best_weights.json")])
        except:
            pass


if __name__ == '__main__':

    while can_it_run():
        nextJob = get_next_job()
        print(nextJob)
        if nextJob is not None:

            run(nextJob)

        else:
            break


