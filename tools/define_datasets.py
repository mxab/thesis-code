""" This script defines the datasets for test/validation/training and saves them to the respective json file"""
import os
import random
from medio import convert_tf
from medio import parse_tf
import json

dict_data = parse_tf.sort_data("/home/d1266/med_data/KORA_tf3")
training_subjects = sorted(dict_data.keys())
dict_data, training_subjects = parse_tf.choose_data(dict_data, training_subjects, 'sequence', '**/*iso_F*')
dict_data, training_subjects = parse_tf.choose_data(dict_data, training_subjects, 'sequence', '**/*iso_W*')
dict_data, training_subjects = parse_tf.choose_data(dict_data, training_subjects, 'label', '**/*liver*')
dict_data, training_subjects = parse_tf.choose_data(dict_data, training_subjects, 'label', '**/*spleen*')

list_images_f = parse_tf.fetch_paths(dict_data, training_subjects, 'sequence', '**/*iso_F*')
list_images_w = parse_tf.fetch_paths(dict_data, training_subjects, 'sequence', '**/*iso_W*')
list_labels_liver = parse_tf.fetch_paths(dict_data, training_subjects, 'label', '**/*liver*')
list_labels_spleen = parse_tf.fetch_paths(dict_data, training_subjects, 'label', '**/*spleen*')

dataset = list(zip(list_images_f, list_images_w, list_labels_liver, list_labels_spleen))

print('chosen subjects: ', len(training_subjects))
print('images fat: ', len(list_images_f))
print('images water: ', len(list_images_w))
print('labels_liver: ', len(list_labels_liver))
print('labels_spleen: ', len(list_labels_spleen))

num_training_data = 125
num_test_data = 35
num_valid_data = len(training_subjects)-num_training_data-num_test_data

# Shuffle the lists
random.seed(230)
random.shuffle(dataset)

print(len(dataset))

# Select training elements
train_dict = dict()
train_dict["images_fat"] = list()
train_dict["images_water"] = list()
train_dict["labels_liver"] = list()
train_dict["labels_spleen"] = list()

start = 0
stop = num_training_data
train_list = dataset[0:num_training_data]

print(len(train_list))

for i in range(len(train_list)):
    train_dict["images_fat"].append(train_list[i][0])
    train_dict["images_water"].append(train_list[i][1])
    train_dict["labels_liver"].append(train_list[i][2])
    train_dict["labels_spleen"].append(train_list[i][3])

print(train_dict["labels_spleen"][-1])
with open(os.path.join("/home/d1266/no_backup/d1266/","train_data.json"),"a") as f:
    json.dump(train_dict, f, indent=4, sort_keys=True)

valid_dict = dict()
valid_dict["images_fat"] = list()
valid_dict["images_water"] = list()
valid_dict["labels_liver"] = list()
valid_dict["labels_spleen"] = list()

start = stop
stop = start+num_valid_data
valid_list = dataset[start:stop]

for i in range(len(valid_list)):
    valid_dict["images_fat"].append(valid_list[i][0])
    valid_dict["images_water"].append(valid_list[i][1])
    valid_dict["labels_liver"].append(valid_list[i][2])
    valid_dict["labels_spleen"].append(valid_list[i][3])

print(valid_dict["labels_spleen"][0])
print(len(valid_dict["images_fat"]))

with open(os.path.join("/home/d1266/no_backup/d1266/","valid_data.json"),"a") as f:
    json.dump(valid_dict, f, indent=4, sort_keys=True)

test_dict = dict()
test_dict["images_fat"] = list()
test_dict["images_water"] = list()
test_dict["labels_liver"] = list()
test_dict["labels_spleen"] = list()

start = stop
stop = start+num_test_data

test_list = dataset[start:stop]

for i in range(len(test_list)):
    test_dict["images_fat"].append(test_list[i][0])
    test_dict["images_water"].append(test_list[i][1])
    test_dict["labels_liver"].append(test_list[i][2])
    test_dict["labels_spleen"].append(test_list[i][3])

print(len(test_dict["images_fat"]))

with open(os.path.join("/home/d1266/no_backup/d1266/","test_data.json"),"a") as f:
    json.dump(test_dict, f, indent=4, sort_keys=True)
