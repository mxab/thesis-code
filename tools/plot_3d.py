import numpy as np
import matplotlib
matplotlib.use("qt4agg")
import nibabel
import os
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--model_dir', default='patchgan6x6',
                    help="Experiment directory containing params.json")
parser.add_argument('--pat', default=1)

args = parser.parse_args()

loadpath = os.path.join('/home/max/Schreibtisch/exp_thesis/', args.model_dir, 'results/npy/pat{}'.format(args.pat))
print(loadpath)

img = np.load(os.path.join(loadpath, "images.npy"))
lbl = np.load(os.path.join(loadpath, "labels.npy"))
pred = np.load(os.path.join(loadpath, "pred.npy"))
lbl = np.expand_dims(lbl, axis=4)
pred = np.expand_dims(pred, axis=4)
print(img.shape, lbl.shape, pred.shape)
data = np.concatenate([img, lbl, pred], axis=4)
data = np.squeeze(data, axis=0)
nibabel.viewers.OrthoSlicer3D(data).show()


